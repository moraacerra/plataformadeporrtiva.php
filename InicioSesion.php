<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="InicioSesion.css">
</head>

<body>
    <form class="form" action="VerificarLogin.php" method="post">
        <p class="title">Login</p>
        <p class="message">Sign in to your account</p>
        <label>
            <input class="input" type="email" name="email" placeholder="" required="">
            <span>Email</span>
        </label>
        <label>
            <input class="input" type="password" name="password" placeholder="" required="">
            <span>Password</span>
        </label>
        <button class="submit" type="submit">Sign In</button>
        <p class="signin">Don't have an account? <a href="Registro.php">Register</a></p>
    </form>
</body>

</html>
