<?php
session_start();

class Password {
    public static function hash($password) {
        return password_hash($password, PASSWORD_DEFAULT, ['cost' => 15]);
    }
    
    public static function verify($password, $hash) {
        return password_verify($password, $hash);
    }
}

// Conexión a la base de datos
$servername = "192.168.56.101:3306";
$username = "admin";
$password = "admin";
$dbname = "Registro_php";

$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar conexión
if ($conn->connect_error) {
    die("Error de conexión: " . $conn->connect_error);
}

$nombre = $_POST['nombre'];
$apellidos = $_POST['apellidos'];
$email = $_POST['email'];
$password = $_POST['password'];

// Hash de la contraseña
$hashed_password = Password::hash($password);

// Verificar si el email ya existe en la base de datos
$sql = "SELECT id FROM usuarios WHERE email='$email'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Email ya registrado, redirigir al registro con un error
    $_SESSION['error'] = "El email ya está registrado.";
    header("Location: Registro.php");
    exit();
} else {
    // Email no registrado, guardar el nuevo usuario en la base de datos
    $sql = "INSERT INTO usuarios (nombre, apellidos, email, hashed_password)
    VALUES ('$nombre', '$apellidos', '$email', '$hashed_password')";
    
    if ($conn->query($sql) === TRUE) {
        // Usuario registrado con éxito, redirigir a mostrar.php
        $_SESSION['id_usuario'] = $conn->insert_id;
        header("Location: Mostrar.php");
        exit();
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

$conn->close();
?>
