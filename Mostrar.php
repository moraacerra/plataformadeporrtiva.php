<?php
session_start();

// Conexión a la base de datos
$servername = "192.168.56.101:3306";
$username = "admin";
$password = "admin";
$dbname = "Registro_php";

$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar conexión
if ($conn->connect_error) {
    die("Error de conexión: " . $conn->connect_error);
}

$id_usuario = $_SESSION['id_usuario'];

$sql = "SELECT * FROM usuarios WHERE id='$id_usuario'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // Obtener los datos del usuario
    $usuario = $result->fetch_assoc();
} else {
    echo "Usuario no encontrado";
}

$conn->close();
session_unset();
session_destroy();


?>

<?php
// Verificar si hay datos de usuario o si el email no es de Gmail
if (!isset($usuario) || !preg_match('/@gmail\.com$/', $usuario['email'])) {
    // Redirigir a la página de registro si no hay datos de usuario o si el email no es de Gmail
    header("Location: Registro.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mostrar Usuario</title>
    <link rel="stylesheet" href="Mostrar.css">
</head>

<body>
    <div class="container">
        <h2>Datos del Usuario</h2>
        <p><strong>Nombre:</strong> <?php echo $usuario['nombre']; ?></p>
        <p><strong>Apellidos:</strong> <?php echo $usuario['apellidos']; ?></p>
        <p><strong>Email:</strong> <?php echo $usuario['email']; ?></p>
    </div>
</body>

</html>
