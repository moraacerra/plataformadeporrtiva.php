<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Registro</title>
  <link rel="stylesheet" href="Registro.css">
</head>

<body>
  <form class="form" action="VerificarRegistro.php" method="post">
    <p class="title">Registro</p>
    <p class="message">Regístrate para acceder a la página</p>
    <div class="flex">
      <label>
        <input class="input" type="text" name="nombre" placeholder="Nombre" required>
      </label>
      <label class="label">
        <input class="input" type="text" name="apellidos" placeholder="Apellidos" required>
      </label>
    </div>
    <label>
      <input class="input" type="email" name="email" placeholder="Correo electrónico" required>
    </label>
    <label>
      <input class="input" type="password" name="password" placeholder="Contraseña" required>
    </label>
    <label>
      <input class="input" type="password" placeholder="Confirmar contraseña" required>
    </label>
    <button class="submit" type="submit">Enviar</button>
    <p class="signin">¿Ya tienes una cuenta? <a href="InicioSesion.php">Inicia sesión</a></p>
  </form>
  <?php
  if (isset($_SESSION['error'])) {
    echo "<p class='error'>{$_SESSION['error']}</p>";
    unset($_SESSION['error']);
  }
  ?>
</body>

</html>
