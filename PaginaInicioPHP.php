<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página de Inicio - Plataforma Deportiva</title>
    <link rel="stylesheet" href="PaginaInicioCSS.css">
</head>
<body>
    <header>
        <div class="scrolling-text">
            <p>"Sumérgete en el apasionante mundo del deporte, disfruta de los mejores eventos en vivo y en directo, todo completamente gratis!"</p>
        </div>
    </header>
    <div class="overlay">
        <div class="content-box">
                <h1 class="tracking-in-expand">Plataforma Deportiva</h1>
                <p class="tracking-in-expand-fwd">¡Disfruta de tus deportes favoritos gratis!</p>
           
            <main>
                <div class="buttons">
                    <button href="" class="login-btn">Iniciar sesión</button>
                    <button href="" class="register-btn">Registrarse</button>
                </div>
            </main>
        </div>
    </div>
    <?php
  if (isset($_SESSION['error'])) {
    echo "<p class='error'>{$_SESSION['error']}</p>";
    unset($_SESSION['error']);
  }
  ?>
</body>
</html>