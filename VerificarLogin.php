<?php
session_start();


// Incluir la definición de la clase Password
class Password {
    public static function hash($password) {
        return password_hash($password, PASSWORD_DEFAULT, ['cost' => 15]);
    }
    
    public static function verify($password, $hash) {
        return password_verify($password, $hash);
    }
}

// Conexión a la base de datos
$servername = "192.168.56.101:3306";
$username = "admin";
$password = "admin";
$dbname = "Registro_php";

$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar conexión
if ($conn->connect_error) {
    die("Error de conexión: " . $conn->connect_error);
}

$email = $_POST['email'];
$password = $_POST['password'];

// Verificar en la base de datos si existe alguien con ese email y contraseña
$sql = "SELECT id, hashed_password FROM usuarios WHERE email='$email'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $hashed_password = $row['hashed_password'];
    
    if (Password::verify($password, $hashed_password)) {
        // Contraseña correcta, redirigir a Mostrar.php
        $_SESSION['id_usuario'] = $row['id'];
        header("Location: Mostrar.php");
        exit();
    } else {
        // Contraseña incorrecta, redirigir a Login.php con error
        $_SESSION['error'] = "Contraseña incorrecta.";
        header("Location: InicioSesion.php");
        exit();
    }
} else {
    // Usuario no encontrado, redirigir a Login.php con error
    $_SESSION['error'] = "Usuario no encontrado.";
    header("Location: InicioSesion.php");
    exit();
}

$conn->close();
?>
